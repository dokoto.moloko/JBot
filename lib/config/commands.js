'use strict';

module.exports = {
    HELP: `Commands Help\n
    /help : Show this help
    /invtunnel : Activate ssh inverse tunnel
    /ngrok [start tunnelName|stop|info tunnelName|infos] (Start ngrok tunnel)`,
    ACTIONS: {
        '/invtunnel': {
            method: '_selectModule',
            path: './trollan/main',
            argv: [],
        },
        '/ngrok': {
            method: '_selectModule',
            path: './ngrok/main',
            argv: ['start', 'stop', 'info', 'infos', 'jenkins', 'ssh'],
        },
        '/help': {
            method: '_sendHelp',
            argv: []
        },
        '/start': {
            method: '_hello',
            argv: []
        }
    }
};
