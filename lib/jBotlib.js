'use strict';


const paramsManager = require('./utils/paramsManager');
const misc = require('./utils/misc');
const TelegramDaemon = require('./modules/telegramAPI/telegramDaemon');
const ProxyModule = require('./modules/proxy');
const cluster = require('cluster');

class JBot {
    constructor(options) {
        this.options = options;
        this.telegramBot = new TelegramDaemon();
    }

    _handleTelegramIncomingMsg(message) {
        new ProxyModule(this.telegramBot, message).run();
    }

    _starTelegramDaemon() {
        this.telegramBot.on('daemon:incoming:mesage', this._handleTelegramIncomingMsg.bind(this));
        this.telegramBot.start();
    }

    _handleClusterExit(worker, code, signal) {
        console.log(`[WORKER] ${worker.process.pid} died`);
        if (!worker.exitedAfterDisconnect) {
            cluster.fork();
        }
    }

    _startClusterMode() {
        if (cluster.isMaster) {
            console.log(`[MASTER] ${process.pid} is running`);
            cluster.fork();
            this._starTelegramDaemon();
            cluster.on('exit', this._handleClusterExit.bind(this));
        } else {
            console.log(`[LAZYGIFTER][WORKER] ${process.pid} is running`);
        }
    }

    run() {
        if (this.options.help || misc.isFalse(this.options)) {
            paramsManager.help();
        } else if (this.options.cluster) {
            this._startClusterMode();
        } else if (this.options.daemon) {
            this._starTelegramDaemon();
        }
    }

}

module.exports = JBot;
