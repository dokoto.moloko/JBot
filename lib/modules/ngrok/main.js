'use strict';

const exec = require('child_process').exec;
const spawn = require('child_process').spawn;
const http = require('http');
const Events = require('events');

const API_HOST = 'localhost';
const API_PORT = 4040;

class Ngrok extends Events {
    constructor(bot, user_id, argv, allowArgv) {
        super();
        this.bot = bot;
        this.user_id = user_id;
        this.argv = argv;
        this.allowArgv = allowArgv;
    }

    _handleNgrokResponseFinish(msg) {
        console.debug('[NGROK] Response %s', JSON.stringify(msg));
        this.bot.sendMessage({
            chat_id: this.user_id,
            text: msg.public_url || msg.msg
        });
    }

    _handleNgrokRequest(response) {
        let msg = {};
        response.setEncoding('utf8');
        response.on('data', chunk => msg = JSON.parse(chunk));
        response.on('end', () => this.emit('ngrokrequest:finish', msg));
    }

    _handleNgrokRequestError(err) {
        console.error('NGROK SEEMS DOWN : %s', JSON.stringify(err));
        this.bot.sendMessage({
            chat_id: this.user_id,
            text: 'NGROK_SEEMS_DOWN'
        });
    }

    _handleCloseNgrokProcess(code, signal) {
        this.bot.sendMessage({
            chat_id: this.user_id,
            text: 'Ngork process closed success'
        });
        APP.ngrokProcess = null;
    }

    _start(tunnelName) {
        if (!APP.ngrokProcess) {
            APP.ngrokProcess = spawn('ngrok', ['start', tunnelName]);
            APP.ngrokProcess.once('close', this._handleCloseNgrokProcess.bind(this));
            if (APP.ngrokProcess) {
                console.log('[NGROK] Process pid %s started success', APP.ngrokProcess.pid);
                this.bot.sendMessage({
                    chat_id: this.user_id,
                    text: 'Ngrok process start success. Please wait 20 seconds and request info about your tunnel'
                });
            }
        } else {
            this.bot.sendMessage({
                chat_id: this.user_id,
                text: 'Ngork is running rigth now. Please request info about it'
            });
        }
    }

    _stop() {
        try {
            if (APP.ngrokProcess) {
                APP.ngrokProcess.kill();
            } else {
                this.bot.sendMessage({
                    chat_id: this.user_id,
                    text: 'No Ngrok process found. Please start a ngrok process.'
                });
            }
        } catch (err) {
            console.error('[NGROK] %s', JSON.stringify(err));
        }
    }


    _getNgrokRequestOptions(method, tunnelName) {
        return {
            hostname: API_HOST,
            port: API_PORT,
            path: (tunnelName) ? sprintf('/api/tunnels/%s', tunnelName) : '/api/tunnels',
            'method': method,
            headers: {
                'Content-Type': 'application/json'
            }
        };
    }

    _ngrokRequest(options) {
        let request = http.request(options, this._handleNgrokRequest.bind(this));
        request.once('error', this._handleNgrokRequestError.bind(this));
        request.end();
    }

    _info(tunnelName) {
        let options = this._getNgrokRequestOptions('GET', tunnelName);
        this.once('ngrokrequest:finish', this._handleNgrokResponseFinish.bind(this));
        this._ngrokRequest(options);
    }

    _infos() {
        let options = this._getNgrokRequestOptions('GET');
        this.once('ngrokrequest:finish', this._handleNgrokResponseFinish.bind(this));
        this._ngrokRequest(options);
    }

    _parseArgs() {
        console.debug('[NGROK] Arguments: %s', JSON.stringify(this.argv));
        if (this.allowArgv.indexOf(this.argv[0]) === -1) {
            throw new Error('BAD_NGROK_COMMAND');
        }
    }

    _proxyCmd() {
        switch (this.argv[0]) {
            case 'start':
                this._start(this.argv[1]);
                break;
            case 'stop':
                this._stop();
                break;
            case 'info':
                this._info(this.argv[1]);
                break;
            case 'infos':
                this._infos();
                break;
        }
    }

    run() {
        this._parseArgs();
        this._proxyCmd();
    }
}

module.exports = Ngrok;
