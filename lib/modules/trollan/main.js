'use strict';

const fs = require('fs');
const Client = require('ssh2').Client;

class Trollan {
    constructor(bot, user_id, argv, allowArgv) {
        this.bot = bot;
        this.user_id = user_id;
        this.argv = argv;
        this.allowArgv = allowArgv;
    }

    /*
     * SSH RSA AUTH
     *
     * SERVIDOR:
     * Aqui los cambios son a nivel de configuracion en /etc/ssh/sshd_conf
     * RSAAuthentication yes
     * PubkeyAuthentication yes
     *
     * CLIENTE:
     * $> ssh-keygen -t rsa
     * $> ssh-copy-id -i ~/.ssh/id_rsa.pub user@host_destino
     */
    ssh_normal_connection() {
        let conn = new Client();
        conn.on('ready', () => {
            console.log('SSH Client :: Ready');
            conn.exec('uptime', (err, stream) => {
                if (err) throw new Error(err);
                stream.on('close', (code, signal) => {
                        console.log('Code : %s Signal: %s', code, signal);
                        conn.end();
                    })
                    .on('data', data => console.log('STDOUT: %s', data))
                    .stderr.on('data', data => console.log('STDERR: %s', data));
            });
        }).connect({
            host: '10.80.80.201',
            port: 22,
            username: 'opvtabletadmin',
            privateKey: fs.readFileSync('./id_rsa'),
            passphrase: 'apolo10',
            agent: process.env.SSH_AUTH_SOCK
        });
    }


    run() {

    }
}

module.exports = Trollan;
