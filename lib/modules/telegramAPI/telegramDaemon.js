'use strict';

const TelegramBot = require('node-telegram-bot');
const TELEGRAM_CONST = require('../../config/telegramAPI');
const EventEmitter = require('events');

class TelegramDaemon extends EventEmitter {
    constructor() {
        super();
        this.telegramBot = new TelegramBot({
            token: TELEGRAM_CONST.TOKEN
        });
    }

    start() {
        console.printf('[TELEGRAM-DAEMON] Started');
        this.telegramBot.on('message', this._handleOnIncoming.bind(this));
        this.telegramBot.start();
    }

    destroy() {
        this.telegramBot.removeAllListeners('message');
    }

    sendPhoto(options) {
        this.telegramBot.sendPhoto(options, () => this.emit('daemon:image:sent'));
    }

    sendMessage(options) {
        this.telegramBot.sendMessage(options, () => this.emit('daemon:message:sent'));
    }

    _handleOnIncoming(message) {
        console.printf('[TELEGRAM-DAEMON] Normal incoming message from telegram');
        console.debug('[TELEGRAM-DAEMON][MESSAGE] %s', JSON.stringify(message));
        if (TELEGRAM_CONST.ALLOW_USERS_ID.indexOf(message.from.id) !== -1) {
            this.emit('daemon:incoming:mesage', message);
        } else {
            console.printf('[TELEGRAM-DAEMON] User id "%s" NO ALLOWED', message.from.id);
            this.emit('daemon:user:noallowed', message);
        }
    }

}

module.exports = TelegramDaemon;
