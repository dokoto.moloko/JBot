'use strict';

const misc = require('./misc');

class ParamsManager {
    constructor() {
        this.rawParams = process.argv;
        this.exclusions = [];
        this.options = {
            help: false,
            verbose: false,
            daemon: false,
            cluster: false,
            mocks: {
                telegram: false
            }
        };
        this._processOptions();
    }

    _excludingParam(param) {
        this.options[param] = (this.rawParams.indexOf('--' + param) !== -1);
        if (this.options[param]) {
            misc.recursiveAssign(this.options, false, this.exclusions.concat(param));
        }

        return this.options[param];
    }

    _addParam(param) {
        this.options[param] = (this.rawParams.indexOf('--' + param) !== -1);
        this.exclusions.push(param);
    }

    _excludingParamWithValues(param) {
        try {
            let regexTpl = new RegExp('--' + param);
            let mockParam = this.rawParams.filter(item => regexTpl.test(item));
            if (mockParam.length > 0) {
                let options = mockParam[0].split('=')[1].split(',');
                for (let i in options) {
                    if (this.options[param].hasOwnProperty(options[i])) {
                        this.options[param][options[i]] = true;
                        misc.recursiveAssign(this.options, false, this.exclusions.concat(options));
                        console.printf('[OPTIONS] Mock %s activated', options[i]);
                    } else {
                        throw new Error(sprintf('[OPTIONS][ERROR] Option "%s" not recognize', options[i]));
                    }
                }

                return (mockParam.length > 0);
            }
        } catch (err) {
            console.error(err.message);
            return true;
        }
    }

    _processOptions(options) {
        this._addParam('cluster');
        this._addParam('verbose');

        if (this._excludingParam('help')) return;
        if (this._excludingParam('daemon')) return;
        if (this._excludingParamWithValues('mocks')) return;

        console.printf('[OPTIONS] ', JSON.stringify(this.options));
    }

    static help() {
        console.printf('====> HELP <==== ');
        console.printf('use: $> lazygifter [options]');
        console.printf('[OPTIONS] --help: This help');
        console.printf('[OPTIONS] --daemon: Start dameon mode');
        console.printf('[OPTIONS] --cluster: Start dameon mode in cluster');
        console.printf('[OPTIONS] --verbose: Verbose trace logger');
        console.printf('[OPTIONS] --mocks=[test]: Start mocks mode. If --mocks is present other params will not be in count');
        console.printf('[OPTIONS][MOCKS] telegram: Disable telegram tranference, and generate an test/web/index.html with de images #Sample: --mocks=telegram');
    }

    get() {
        return this.options;
    }

}


module.exports = ParamsManager;
