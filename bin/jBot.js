#!/usr/bin/env node

'use strict';

const ParamsManager = require('../lib/utils/paramsManager');
const SetGlobals = require('../lib/utils/setGlobals');
const JBot = require('../lib/jBotlib');

SetGlobals.run();
let params = new ParamsManager();
let jBot = new JBot(params.get());

jBot.run();
